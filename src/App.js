import React, { Component } from 'react'
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from 'react-router-dom';
import Login from './component/Login';
import Signup from './component/Signup';
import UserLogged from './component/UserLogged';

class App extends Component {
  render(){
    return (
        <Router>
          <Switch>
            <Route path="/" exact component={Login} />
            <Route path="/signup" component={Signup} />
            <Route path="/user" component={UserLogged} />
          </Switch>
        </Router>
    )
  }
}

export default App