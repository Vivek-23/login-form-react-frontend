import { Link } from "react-router-dom";
import React, { Component } from "react";
import { Form, FormGroup, Alert, Input, FormText } from "reactstrap";
import "./css/Signup.css";
import Axios from "axios";

export default class Signup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      email: "",
      password: "",
      repassword: "",
      message: "",
      visible: false,
      emailValid: false,
      passwordValid: false
    };
  }

  changeName = (event) => {
    this.setState({
      name: event.target.value,
    });
  };
  changeEmail = (event) => {
    this.setState({
      email: event.target.value,
    });
  };
  changePassword = (event) => {
    this.setState({
      password: event.target.value,
    });
  };
  changeRepassword = (event) => {
    this.setState({
      repassword: event.target.value,
    });
  };

  fetchUserDetails = async () => {
    try {
      let response = await Axios.post(
        "https://vivek-login-form-react.herokuapp.com/signup",
        {
          name: this.state.name,
          email: this.state.email,
          password: this.state.password,
        }
      );
      return response;
    } catch (err) {
      throw err;
    }
  };

  componentDidMount() {
    document.title = "Signup Page";
  }

  handleSubmit = async (event) => {
    event.preventDefault();
    if (this.state.password !== this.state.repassword) {
      this.setState({
        message: "Password are not matched",
        emailValid:false,
        passwordValid: true,
        password: "",
        repassword: "",
      });
    } else {
      try {
        let response = await this.fetchUserDetails();
        if (response.status === 200) {
          this.setState({
            message: "Register Successfully",
            emailValid: false,
            passwordValid: false,
            visible: true,
          });
          setTimeout(() => {
            window.location.href = "/";
          }, 1000);
        }
      } catch (error) {
        this.setState({
          message: error.response.data.status,
          emailValid: true,
          passwordValid: false
        });
      }
    }
  };

  render() {
    const { visible, message, emailValid, passwordValid } = this.state;
    return (
      <div className="container">
        <title>Sigup Page</title>
        <Alert color="primary" isOpen={visible}>
          {message}
        </Alert>
        <div className="signup-container">
          <h1>Sign up</h1>
          <Form className="signup-form" onSubmit={this.handleSubmit}>
            <FormGroup id="standard-basic">
              <Input
                type="text"
                onChange={this.changeName}
                placeholder="Enter your name"
                required
              />
            </FormGroup>
            <FormGroup id="standard-basic">
              <Input
                type="email"
                onChange={this.changeEmail}
                required
                placeholder="Enter email"
                invalid={emailValid}
              />
              <FormText>{(emailValid)? message : ''}</FormText>
            </FormGroup>
            <FormGroup id="standard-basic">
              <Input
                type="password"
                onChange={this.changePassword}
                required
                placeholder="Enter password"
                invalid={passwordValid}
              />
              <FormText>{(passwordValid)? message : ''}</FormText>
            </FormGroup>
            <FormGroup id="standard-basic">
              <Input
                type="password"
                onChange={this.changeRepassword}
                required
                placeholder="Retype Password"
                invalid={passwordValid}
              />
              <FormText>{(passwordValid)? message : ''}</FormText>
            </FormGroup>
            <br />
            <button id="btn" variant="contained" color="primary" type="submit">
              Register
            </button>
            <br />
            <div id="signup-link">
              <Link to="/" id="signup-link">
                I am already member
              </Link>
            </div>
          </Form>
        </div>
      </div>
    );
  }
}
