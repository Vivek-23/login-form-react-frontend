import axios from "axios";
import React, { Component } from "react";
import { Button } from "reactstrap";
import { Redirect} from "react-router-dom";

export class UserLogged extends Component {
  constructor(props) {
    super(props);

    this.state = {
      id: 0,
      name: "",
      email: "",
      navigate: false,
    };
  }

  fetchUser = async (config) => {
    try {
      let response = await axios.get("https://vivek-login-form-react.herokuapp.com/user", config);
      return await response.data;
    } catch (error) {
      throw Error(error.response);
    }
  };

  handleLogout = () => {
      localStorage.clear();
      this.setState({navigate: true});
  } 

  componentDidMount() {
    const config = {
      headers: {
        Authorization: localStorage.getItem("token"),
      },
    };

    document.title = `Dashboard`
    
    this.fetchUser(config)
    .then((responseData) => {
      this.setState({
        id: responseData.id,
        name: responseData.name,
        email: responseData.email,
        });
      })
      .catch((err) => {
        return <Redirect to="/" />  
      });
  }
  render() {
      const {navigate} = this.state;
      if(navigate){
            return <Redirect exact to ='/' />
      }
    return (
      <div>
        <h1>Hello, {this.state.name}.</h1>

        <Button color="primary" onClick={this.handleLogout}>
            Logout
        </Button>
      </div>
    );
  }
}

export default UserLogged;
