import React, { Component } from "react";
import { Form, FormGroup, Alert, Button, Input, FormText } from "reactstrap";
import { Link } from "react-router-dom";
import "./css/login.css";
import Axios from "axios";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      message: "",
      visible: false,
      inputValid: false,
    };
  }

  emailChange = (event) => {
    this.setState({
      email: event.target.value,
    });
  };

  passwordChange = (event) => {
    this.setState({
      password: event.target.value,
    });
  };

  fetchUserDetails = async () => {
    try {
      let response = await Axios.post(
        "https://vivek-login-form-react.herokuapp.com/login",
        {
          email: this.state.email,
          password: this.state.password,
        }
      );
      return response;
    } catch (err) {
      throw err;
    }
  };

  handleSubmit = async (event) => {
    event.preventDefault();
    try {
      let response = await this.fetchUserDetails();
      localStorage.setItem("token", response.data.userDetails.token);
      this.setState({
        message: "Login Successfully",
        visible: true,
      });
      setTimeout(() => {
        window.location.href = "/user";
      }, 1000);
    } catch (err) {
      this.setState({
        message: err.response.data.message,
        inputValid: true,
      });
    }
  };

  componentDidMount() {
    document.title = "Login Page";
  }

  render() {
    const { visible, message, inputValid } = this.state;
    return (
      <div className="container">
        <title>Login</title>
        <Alert color="primary" isOpen={visible}>
          {message}
        </Alert>
        <div className="login-container">
          <h1>Login</h1>
          <Form className="login-form" onSubmit={this.handleSubmit}>
            <FormGroup id="standard-basic">
              <Input
                type="email"
                onChange={this.emailChange}
                required
                placeholder="Enter email"
                invalid={inputValid}
              />
              <FormText>{message}</FormText>
            </FormGroup>
            <FormGroup id="standard-basic">
              <Input
                type="password"
                onChange={this.passwordChange}
                required
                placeholder="Enter password"
              />
            </FormGroup>
            <Button
              id="login-btn"
              variant="contained"
              color="primary"
              type="submit"
            >
              Login
            </Button>
            <div id="signup-link">
              Doesn't have an account?
              <br />
              <Link to="/signup" id="login-link">
                Signup Here
              </Link>
            </div>
          </Form>
        </div>
      </div>
    );
  }
}

export default Login;
